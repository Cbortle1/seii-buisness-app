package application;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/* Launches the EmployeeView GUI.
 * 
 */
public class Main extends Application {
	public static final String APPLICATION_TITLE = "Business Management";
	public static final String ENTRY_VIEW_NAME = "../edu\\westga\\cs3212\\businessmanagement\\view\\EntryView.fxml";
	public static final String EMPLOYEE_LIST_VIEW_NAME = "../edu\\westga\\cs3212\\businessmanagement\\view\\EmployeeListView.fxml";
	public static final String FINANCES_VIEW_NAME = "../edu\\westga\\cs3212\\businessmanagement\\view\\FinancesView.fxml";
	public static final String ADD_EMPLOYEE_VIEW_NAME = "../edu\\westga\\cs3212\\businessmanagement\\view\\AddEmployeeView.fxml";
	public static final String ADD_PROFIT_VIEW_NAME = "../edu\\westga\\cs3212\\businessmanagement\\view\\AddProfitView.fxml";
	public static final String DETAILED_EMPLOYEE_VIEW_NAME = "../edu\\westga\\cs3212\\businessmanagement\\view\\DetailedEmployeeView.fxml";

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			URL url = getClass().getResource(ENTRY_VIEW_NAME);
			AnchorPane root = FXMLLoader.load(url);
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
