package application;

import java.net.URL;

import javafx.fxml.FXMLLoader;

/**
 * Creates a set of FXML Loaders which can be retrieved using its associated get
 * method.
 * 
 * @author Group 3
 * @version 2018
 */
public class Loaders {
	private static FXMLLoader ADD_EMPLOYEE_FXML_LOADER;
	private static FXMLLoader FINANCES_FXML_LOADER;

	public static final String ADD_EMPLOYEE_FXML_VIEW = "../edu\\westga\\cs3212\\businessmanagement\\view\\AddEmployeeView.fxml";
	public static final String FINANCES_FXML_VIEW = "../edu\\westga\\cs3212\\businessmanagement\\view\\FinancesView.fxml";

	/**
	 * Initializes all loaders, except the main, used in the system.
	 * 
	 * @precondition none
	 * @postcondition all loaders are initialized.
	 */
	public Loaders() {
		try {
			this.initializeLoader(ADD_EMPLOYEE_FXML_LOADER, Loaders.class.getResource(Loaders.ADD_EMPLOYEE_FXML_VIEW));
			this.initializeLoader(FINANCES_FXML_LOADER, Loaders.class.getResource(Loaders.FINANCES_FXML_VIEW));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initializeLoader(FXMLLoader selectedLoader, URL viewLocation) throws Exception {
		selectedLoader = new FXMLLoader();
		selectedLoader.setLocation(viewLocation);
		selectedLoader.load();
	}

	public FXMLLoader getAddEmployeeViewLoader() {
		return ADD_EMPLOYEE_FXML_LOADER;
	}

	public FXMLLoader getFinancesViewLoader() {
		return FINANCES_FXML_LOADER;
	}

}
