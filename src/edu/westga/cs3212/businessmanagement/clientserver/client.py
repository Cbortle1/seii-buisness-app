import zmq
import json
import testDummy

def runClient():
    # Prepare our context and sockets
    # Sending Data to Python Server
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    print("connecting to server...")
    socket.connect("tcp://127.0.0.1:5555")
    
    listOfTests = testDummy.testList
    
    for index in range(len(listOfTests)):
        #  Wait for next request from java client
        print("waiting for input...")
        json_message = json.dumps(listOfTests[index])
        
        print("sending message...")
        socket.send_string(json_message)
        
        json_response = socket.recv_string()
        response = json.loads(json_response)
        
        request = json.loads(json_message)
        
        print("Received reply %s [%s]" % (request, response))

    print("sending exit message...")
    message = {'content': 'exit'}
    json_message = json.dumps(message)
    socket.send_string(json_message)
        
if(__name__ == "__main__"):
   runClient()