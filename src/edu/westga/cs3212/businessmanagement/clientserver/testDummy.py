import zmq
import time
import json
import keywords


testList = []
def runTests():
    
    print("initializing tests")
    #Positive Tests
    
    #Tests the add employee function of the server.
    test1 = {keywords.CONTENT_VARIABLE : keywords.ADD_EMPLOYEE, keywords.NAME_VARIABLE : "Bob", keywords.ID_VARIABLE : '324256', keywords.PIN_VARIABLE : "3840", keywords.EXPECTED_OUTCOME : keywords.TEST_PASS}
    
    #Tests the delete employee function of the server.
    test2 = {keywords.CONTENT_VARIABLE : keywords.DELETE_EMPLOYEE, keywords.ID_VARIABLE : '324256', keywords.EXPECTED_OUTCOME : keywords.TEST_PASS}
    
    #Tests the return employee function of the server.
    test3 = {keywords.CONTENT_VARIABLE : keywords.RETURN_EMPLOYEE, keywords.NAME_VARIABLE : "Phillip", keywords.EXPECTED_OUTCOME : keywords.TEST_PASS}
    
    #Tests the return all employees function  of the server.
    test4 = {keywords.CONTENT_VARIABLE : keywords.RETURN_ALL_EMPLOYEES, keywords.EXPECTED_OUTCOME : keywords.TEST_PASS}
    
    #Tests the add profit information function of the server.
    test5 = {keywords.CONTENT_VARIABLE : keywords.ADD_PROFIT_INFORMATION, keywords.PROFIT_VARIABLE : "6,554.87", keywords.EXPECTED_OUTCOME : keywords.TEST_PASS}
    
    #Tests the request profit information function of the server.
    test6 = {keywords.CONTENT_VARIABLE : keywords.REQUEST_PROFITS, keywords.EXPECTED_OUTCOME : keywords.TEST_PASS}
    
    #Negative Tests
    
    #Tests that the delete employee function prints the format is invalid when an id is not provided. 
    test7 = {keywords.CONTENT_VARIABLE : keywords.DELETE_EMPLOYEE, keywords.EXPECTED_OUTCOME : keywords.TEST_FAIL}
    
    #Tests that the add employee function prints the format is invalid when only a name is provided.
    test8 = {keywords.CONTENT_VARIABLE : keywords.ADD_EMPLOYEE, keywords.NAME_VARIABLE : "Jeff", keywords.EXPECTED_OUTCOME : keywords.TEST_FAIL}
    
    #Tests that the add employee function prints the format is invalid when only the id is provided.
    test9 = {keywords.CONTENT_VARIABLE : keywords.ADD_EMPLOYEE, keywords.ID_VARIABLE : '324256', keywords.EXPECTED_OUTCOME : keywords.TEST_FAIL}
    
    #Tests that the add employee function prints the format is invalid when neither the id, nor a name is provided.
    test10 = {keywords.CONTENT_VARIABLE : keywords.ADD_EMPLOYEE, keywords.EXPECTED_OUTCOME : keywords.TEST_FAIL}
    
    #Tests that the return employee function prints the format is invalid when a name is not provided.
    test11 = {keywords.CONTENT_VARIABLE : keywords.RETURN_EMPLOYEE, keywords.EXPECTED_OUTCOME : keywords.TEST_FAIL}
    
    #Tests that the add profit information function prints the format is invalid when a profit is not provided.
    test12 = {keywords.CONTENT_VARIABLE : keywords.ADD_PROFIT_INFORMATION, keywords.EXPECTED_OUTCOME : keywords.TEST_FAIL}
    
    
    
    #Adding positive tests
    testList.append(test1)
    testList.append(test2)
    testList.append(test3)
    testList.append(test4)
    testList.append(test5)
    testList.append(test6)
    
    #Adding negative tests
    testList.append(test7)
    testList.append(test8)
    testList.append(test9)
    testList.append(test10)
    testList.append(test11)
    testList.append(test12)
    
    print("tests initialized")