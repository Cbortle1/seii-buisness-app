from threading import Thread
import time
from server import runServer
from client import runClient
from testDummy import runTests

def main():
    server = Thread(target=runServer)
    client = Thread(target=runClient)
    tests = Thread(target=runTests)
    print("main - starting server")
    server.start()
    print("main - waiting for server to start")
    time.sleep(3)
    print("main - starting client")
    tests.start()
    
    time.sleep(2)
    client.start()
    
    while(server.is_alive() or client.is_alive()):
        time.sleep(1)
        
    return
    
    
if(__name__ == "__main__"):
    main()