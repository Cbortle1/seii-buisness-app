import zmq
import time
import json
import keywords

INVALID_FORMAT = "format is invalid"
INVALID_INPUT = "this input is invalid"

def runServer():
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://127.0.0.1:5555")
    
    while True:
        #  Wait for next request from client
        print("waiting for message...")
        json_message = socket.recv_string()
        
        response_message = 'invalid input'
        
        
        message = json.loads(json_message)
        print("Received request: %s" % message[keywords.CONTENT_VARIABLE])
        
        #Messages will be replaced by code that operates the server based on input
        if(message[keywords.CONTENT_VARIABLE] == keywords.DELETE_EMPLOYEE):
            
            if(message.get(keywords.ID_VARIABLE, INVALID_INPUT) == INVALID_INPUT):
                response_message = INVALID_FORMAT
            else:
                response_message = 'employee with ID: ' + message[keywords.ID_VARIABLE] + ' deleted'
        
        if(message[keywords.CONTENT_VARIABLE] == keywords.ADD_EMPLOYEE):
            
            if(message.get(keywords.NAME_VARIABLE, INVALID_INPUT) == INVALID_INPUT or message.get(keywords.ID_VARIABLE, INVALID_INPUT) == INVALID_INPUT):
                response_message = INVALID_FORMAT
            else:
                response_message = message[keywords.NAME_VARIABLE] + ' added with ID: ' + message[keywords.ID_VARIABLE]

        if(message[keywords.CONTENT_VARIABLE] == keywords.RETURN_EMPLOYEE):
            
            if(message.get(keywords.NAME_VARIABLE, INVALID_INPUT) == INVALID_INPUT):
                response_message = INVALID_FORMAT
            else:
                response_message = 'Employee ' + message[keywords.NAME_VARIABLE] + " found"
            
        if(message[keywords.CONTENT_VARIABLE] == keywords.RETURN_ALL_EMPLOYEES):
            
            response_message = 'Steve, Donna, Phillip'
            
        if(message[keywords.CONTENT_VARIABLE] == keywords.ADD_PROFIT_INFORMATION):
            
            if(message.get(keywords.PROFIT_VARIABLE, INVALID_INPUT) == INVALID_INPUT):
                response_message = INVALID_FORMAT
            else:
                response_message = '$' + message[keywords.PROFIT_VARIABLE] + ' in profits added'
            
        if(message[keywords.CONTENT_VARIABLE] == keywords.REQUEST_PROFITS):
            
            response_message = 'Total Profits: $49,243.42'
            
        #  Do some 'work'
        time.sleep(1)
    
        #  Send reply back to client
        json_response = json.dumps(response_message)
        socket.send_string(json_response)
        
if(__name__ == "__main__"):
   runServer()