/**
 * 
 */
package edu.westga.cs3212.businessmanagement.view_model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import application.Main;
import edu.westga.cs3212.businessmanagement.controller.EmployeeController;
import edu.westga.cs3212.businessmanagement.model.Employee;
import edu.westga.cs3212.businessmanagement.model.Profit;
import edu.westga.cs3212.businessmanagement.model.ProfitLog;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * 
 * Sets actions to view components
 * 
 * @author Caleb Farara, Justin Allen
 *
 */
public class FinancesViewModel {

	private static final String TRANSACTIONS_KEY = "transactions";
	private static final String PROFITS_KEY = "profits";
	private static final String PROFIT_LOG_KEY = "profitlog";

	@FXML
	private Button btnAddProfit, btnBack;

	@FXML
	private ListView<Profit> gainsListView;

	@FXML
	private Label lblTransactions, lblProfits, lblCosts;

	private ProfitLog profitLog;
	private int transactions;
	private double profits;
	private Preferences prefs;

	@FXML
	public void initialize() {
		this.prefs = Preferences.userNodeForPackage(this.getClass());
		this.restoreUIState();
		this.showTotalLaborCosts();
	}

	@FXML
	public void launchAddProfit(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource(Main.ADD_PROFIT_VIEW_NAME));
		AnchorPane root;
		try {
			root = loader.load();
			Stage newStage = new Stage();
			newStage.setScene(new Scene(root));
			newStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AddProfitViewModel addProfitViewModel = loader.getController();
		ScheduledService<Void> service = new ScheduledService<Void>() {

			@Override
			protected Task<Void> createTask() {
				Task<Void> task = new Task<Void>() {

					@Override
					protected Void call() throws Exception {
						while (true) {
							if (addProfitViewModel.profitAdded()) {
								break;
							} else if (addProfitViewModel.isDone()) {
								this.cancel();
								break;
							} else {
								Thread.sleep(1000);
							}
						}
						return null;
					}
				};
				task.setOnSucceeded(notUsed -> {
					Platform.runLater(() -> {
						profits += addProfitViewModel.getPofitLog().get(0).getProfit();
						++transactions;
						updateListView(addProfitViewModel.getProfitLog());
						lblProfits.setText(String.format("$%.2f", profits));
						lblTransactions.setText(transactions + "");
					});
				});
				addProfitViewModel.resetProfitAdded();
				return task;
			}
		};
		service.start();
		service.setPeriod(Duration.seconds(2));
	}

	@FXML
	public void goBack(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(Main.ENTRY_VIEW_NAME));
			AnchorPane root = loader.load();
			Stage previousStage = new Stage();
			previousStage.setScene(new Scene(root));
			previousStage.show();
			this.close();
		} catch (IOException e) {

		}
	}

	@FXML
	public void remove() {
		int selectedIndex = this.gainsListView.getSelectionModel().getSelectedIndex();
		this.profitLog.getAllProfits().remove(selectedIndex);
		--this.transactions;
		this.lblTransactions.setText(this.transactions + "");
		Profit selectedProfit = this.gainsListView.getSelectionModel().getSelectedItem();
		this.profits -= selectedProfit.getProfit();
		System.out.println(this.gainsListView.getSelectionModel().getSelectedItem());
		this.lblProfits.setText(String.format("$%.2f", this.profits));
		this.updateListView();
	}

	private void close() {
		Stage current = (Stage) this.btnAddProfit.getScene().getWindow();
		current.close();
	}

	private void updateListView(ProfitLog profitLog) {
		this.profitLog.getAllProfits().addAll(profitLog.getAllProfits());
		List<Profit> finances = new ArrayList<Profit>(this.profitLog.getAllProfits());
		ObservableList<Profit> profitsObservable = FXCollections.observableArrayList(finances);
		this.gainsListView.setItems(profitsObservable);
		this.updatePreferences();
	}

	private void updateListView() {
		List<Profit> finances = new ArrayList<Profit>(this.profitLog.getAllProfits());
		ObservableList<Profit> profitsObservable = FXCollections.observableArrayList(finances);
		this.gainsListView.setItems(profitsObservable);
		this.updatePreferences();
	}

	/**
	 * Updates UI components to the preferences.
	 * 
	 */
	private void updatePreferences() {
		this.prefs.putDouble(PROFITS_KEY, this.profits);
		this.prefs.putInt(TRANSACTIONS_KEY, this.transactions);
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		byte[] profitsBytes;
		ObjectOutputStream output;
		try {
			output = new ObjectOutputStream(byteStream);
			output.writeObject(this.profitLog);
			output.flush();
			profitsBytes = byteStream.toByteArray();
			this.prefs.putByteArray(PROFIT_LOG_KEY, profitsBytes);
		} catch (IOException e) {
			this.showErrorMessage(e);
		}
	}

	/**
	 * Restores the previous state of the UI components.
	 * 
	 */
	private void restoreUIState() {
		this.profits = this.prefs.getDouble(PROFITS_KEY, 0);
		this.transactions = this.prefs.getInt(TRANSACTIONS_KEY, 0);
		byte[] profitLogBytes = this.prefs.getByteArray(PROFIT_LOG_KEY, null);
		if (profitLogBytes != null) {
			try {
				ByteArrayInputStream byteStream = new ByteArrayInputStream(profitLogBytes);
				ObjectInputStream objInput = new ObjectInputStream(byteStream);
				this.profitLog = (ProfitLog) objInput.readObject();
				this.updateListView();

				this.lblProfits.setText("$" + this.profits);
				this.lblTransactions.setText(this.transactions + "");
			} catch (IOException | ClassNotFoundException e) {
				this.showErrorMessage(e);
			}
		} else {
			this.profitLog = new ProfitLog();
		}

	}

	private void showTotalLaborCosts() {
		int totalLaborCosts = 0;
		byte[] bytes = this.prefs.getByteArray(EmployeeListViewModel.CONTROLLER_KEY, null);
		if (bytes != null) {
			try {
				ByteArrayInputStream input = new ByteArrayInputStream(bytes);
				ObjectInputStream byteInput = new ObjectInputStream(input);
				EmployeeController controller = (EmployeeController) byteInput.readObject();
				for (Employee current : controller.getEmployees()) {
					totalLaborCosts += current.getWage();
				}
			} catch (IOException | ClassNotFoundException e) {
			}
		}
		this.lblCosts.setText(totalLaborCosts + "");
	}

	private void showErrorMessage(Exception e) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setHeaderText("Error!");
		alert.setContentText(e.getMessage());
		alert.showAndWait();
	}

}
