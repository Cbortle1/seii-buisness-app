package edu.westga.cs3212.businessmanagement.view_model;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DetailedEmployeeViewModel {

	@FXML
	private Label lblName;

	@FXML
	private Label lblID, lblWage, lblPin;

	private boolean removed, done;

	/**
	 * Sets all text fields un-editable.
	 * 
	 */
	@FXML
	public void initialize() {
		this.removed = false;
		this.done = false;
	}

	@FXML
	public void remove(ActionEvent event) {
		this.removed = true;
		this.close();
	}

	@FXML
	public void done(ActionEvent event) {
		this.close();
	}

	public boolean done() {
		return this.done;
	}

	public boolean removed() {
		return this.removed;
	}

	public void resetRemoved() {
		this.removed = false;
	}

	/**
	 * Sets the Name label.
	 * 
	 * @param name
	 *            The employees name.
	 */
	public void setLableName(String name) {
		this.lblName.setText(name);
	}

	/**
	 * Set the id text field.
	 * 
	 * @param id
	 *            The employee's id.
	 */
	public void setIDText(int id) {
		this.lblID.setText(id + "");
	}

	/**
	 * Sets the wage text field.
	 * 
	 * @param wage
	 *            The employee's wage.
	 */
	public void setWageText(int wage) {
		this.lblWage.setText(wage + "");
	}

	/**
	 * Sets the pin text field.
	 * 
	 * @param pin
	 *            The employee's pin.
	 */
	public void setPinText(int pin) {
		this.lblPin.setText(pin + "");
	}

	public void close() {
		Stage current = (Stage) this.lblName.getScene().getWindow();
		current.close();
	}
}