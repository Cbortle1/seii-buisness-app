/**
 * 
 */
package edu.westga.cs3212.businessmanagement.view_model;

import java.time.LocalDate;

import edu.westga.cs3212.businessmanagement.model.Profit;
import edu.westga.cs3212.businessmanagement.model.ProfitLog;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * @author Caleb Farara, Justin Allen
 *
 */
public class AddProfitViewModel {

	@FXML
	private Button btnAddProfit;

	@FXML
	private DatePicker datePickerDate;

	@FXML
	private TextField txtProfit;

	@FXML
	private Button btnBack;
	
	private ProfitLog profitLog;
	
	private boolean profitAdded, done;
	
	/**
	 * Initializes all global variables.
	 * 
	 */
	@FXML
	public void initialize() {
		this.profitLog = new ProfitLog();
		this.profitAdded = false;
		this.done = false;
	}
	
	/**
	 * Adds the gain to the profit log.
	 * 
	 */
	@FXML
	public void addGain() {
		try {
		this.profitLog.getAllProfits().clear();
		LocalDate date = this.datePickerDate.getValue();
		double profit = Double.parseDouble(this.txtProfit.getText());
		Profit profitObj = new Profit(date.toString(), profit);
		this.profitLog.addProfit(profitObj);
		this.profitAdded = true;
		} catch (IllegalArgumentException e) {
			this.showErrorMessage(e);
		}
	}
	
	/**
	 * Closes the add gain dialog.
	 * 
	 */
	@FXML
	void goBack() {
		this.done = true;
		this.close();
	}
	/**
	 * Returns the profit log.
	 * 
	 * @return The profit log.
	 */
	public ProfitLog getProfitLog() {
		return this.profitLog;
	}
	
	/**
	 * Determines if the user has added a profit.
	 * 
	 * @return True if the user has clicked add profit, false otherwise.
	 */
	public boolean profitAdded() {
		return this.profitAdded;
	}
	
	/**
	 * Determines if the user is done with the dialog.
	 * 
	 * @return True if the user has clicked back, false otherwise.
	 */
	public boolean isDone() {
		return this.done;
	}
	
	/**
	 * Resets profit added to false.
	 * 
	 */
	public void resetProfitAdded() {
		this.profitAdded = false;
	}
	
	/**
	 * Returns the profit log.
	 * @return
	 */
	public ProfitLog getPofitLog() {
		return this.profitLog;
	}

	private void close() {
		Stage current = (Stage) this.btnAddProfit.getScene().getWindow();
		current.close();
	}
	
	private void showErrorMessage(Exception e) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setHeaderText("Error!");
		alert.setContentText(e.getMessage());
		alert.showAndWait();
	}
}
