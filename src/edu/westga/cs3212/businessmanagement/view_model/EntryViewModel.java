package edu.westga.cs3212.businessmanagement.view_model;

import java.io.IOException;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

//TODO Finish styling entry view.
//TODO Implement EntryView functionality.
public class EntryViewModel {

	@FXML
	private ImageView tieIcon;
	
	@FXML
	private ImageView dollarSign;

	@FXML
	public void initialize() {
		Image employeeImage = new Image("resources\\tie_icon.png");
		this.tieIcon.setImage(employeeImage);
		this.tieIcon.setCache(true);
		Image financesImage = new Image("resources\\dollar-sign.png");
		this.dollarSign.setImage(financesImage);
		this.dollarSign.setCache(true);
	}

	@FXML
	public void launchEmployeeView(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(Main.EMPLOYEE_LIST_VIEW_NAME));
			AnchorPane root = loader.load();
			Stage newStage = new Stage();
			newStage.setScene(new Scene(root));
			newStage.show();
			this.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@FXML
	public void launchFinancesView(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(Main.FINANCES_VIEW_NAME));
			AnchorPane root = loader.load();
			Stage newStage = new Stage();
			newStage.setScene(new Scene(root));
			newStage.show();
			this.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void close() {
		Stage current = (Stage) this.tieIcon.getScene().getWindow();
		current.close();
	}

}
