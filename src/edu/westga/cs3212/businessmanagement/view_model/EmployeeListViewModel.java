package edu.westga.cs3212.businessmanagement.view_model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;

import application.Main;
import edu.westga.cs3212.businessmanagement.controller.EmployeeController;
import edu.westga.cs3212.businessmanagement.model.Employee;
import javafx.application.Platform;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Sets actions to view components
 * 
 * @author Justin Allen
 * @version 1.0
 *
 */
public class EmployeeListViewModel {

	public static final String CONTROLLER_KEY = "controller";

	@FXML
	private Button btnAddEmployee;

	@FXML
	private Button btnBack;

	@FXML
	private ListView<Employee> employeeListView;

	private EmployeeController employeeController;

	private Employee currentEmployee;

	private Preferences prefs;

	/**
	 * Initializes the employee controller.
	 * 
	 */
	@FXML
	public void initialize() {
		this.prefs = Preferences.userNodeForPackage(this.getClass());
		this.restoreUIState();
		this.setOnListItemClick();

	}

	/**
	 * Launches the add employee dialog and listens for new employees added.
	 * 
	 * @param event
	 *            not used
	 */
	@FXML
	public void launchAddEmployee(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource(Main.ADD_EMPLOYEE_VIEW_NAME));
		AnchorPane root;
		try {
			root = loader.load();
			Stage newStage = new Stage();
			Scene scene = new Scene(root);
			newStage.setScene(scene);
			newStage.show();
			AddEmployeeViewModel ad = loader.getController();
			// Start listen for new employees thread.
			ScheduledService<Void> service = new ScheduledService<Void>() {

				@Override
				protected Task<Void> createTask() {
					Task<Void> task = new Task<Void>() {

						@Override
						protected Void call() throws InterruptedException {
							while (true) {
								if (ad.employeeAdded()) {
									break;
								} else if (ad.isDone()) {
									this.cancel();
									break;
								} else {
									Thread.sleep(500);
								}
							}
							ad.resetEmployeeAdded();
							return null;
						}
					};
					// Update the UI after the task is completed.
					task.setOnSucceeded((notUsed) -> {
						Platform.runLater(() -> {
							EmployeeListViewModel.this.updateListView(ad.getController());
						});
					});
					return task;
				}
			};
			// Start the service and restart 1 second after completion.
			service.start();
			service.setPeriod(Duration.seconds(1));
		} catch (IOException e) {
			this.showErrorMessage(e);
		}
	}

	/**
	 * Goes back to main entry view.
	 * 
	 * @param event
	 *            not used
	 */
	@FXML
	public void goBack(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(Main.ENTRY_VIEW_NAME));
			AnchorPane root = loader.load();
			Stage previousStage = new Stage();
			previousStage.setScene(new Scene(root));
			previousStage.show();
			this.close();
		} catch (IOException e) {
			this.showErrorMessage(e);
		}
	}

	/**
	 * Launches the detailed employee dialog and listens for changes.
	 * 
	 */
	private void setOnListItemClick() {
		FXMLLoader loader = new FXMLLoader(Main.class.getResource(Main.DETAILED_EMPLOYEE_VIEW_NAME));
		AnchorPane root;
		DetailedEmployeeViewModel detailedVM;
		Stage stage = new Stage();
		try {
			root = loader.load();
			stage.setScene(new Scene(root));
		} catch (IOException e) {
			this.showErrorMessage(e);
		}
		detailedVM = loader.getController();

		Task<Void> task = new Task<Void>() {

			@Override
			protected Void call() throws Exception {

				while (true) {
					// If the user clicks the remove button remove the employee from the controller.
					if (detailedVM.removed()) {
						EmployeeListViewModel.this.employeeController.removeEmployeeByID(currentEmployee.getId());
						break;

					} else if (detailedVM.done()) {
						this.cancel();

					} else {
						Thread.sleep(1000);
					}
				}
				return null;

			}
		};
		task.setOnSucceeded(notUsed -> {
			Platform.runLater(() -> {
				if (detailedVM.removed()) {
					task.cancel();
				}
				EmployeeListViewModel.this.updateListView(employeeController);
				return;

			});
		});
		this.employeeListView.getSelectionModel().selectedItemProperty()
				.addListener((observableValue, oldEmployee, newEmployee) -> {
					detailedVM.setIDText(newEmployee.getId());
					detailedVM.setWageText(newEmployee.getWage());
					detailedVM.setPinText(newEmployee.getPin());
					detailedVM.setLableName(newEmployee.getName());
					stage.show();
					currentEmployee = newEmployee;
					Thread thread = new Thread(task);
					thread.setDaemon(true);
					thread.start();
				});

	}

	/**
	 * Updated the list view with newly added employees.
	 * 
	 * @param addVM
	 *            The AddEmployeeViewModel instance that the list view will use to
	 *            set its content.
	 */
	private void updateListView(EmployeeController controller) {
		this.employeeController.addAllFrom(controller);
		List<Employee> employees = new ArrayList<Employee>(this.employeeController.getEmployees());
		Collections.sort(employees);
		ObservableList<Employee> emps = FXCollections.observableArrayList(employees);
		this.employeeListView.setItems(emps);
		this.updatePreferences();
	}

	/**
	 * Updates the application preferences.
	 * 
	 */
	private void updatePreferences() {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		byte[] controllerBytes;
		ObjectOutputStream output;
		try {
			output = new ObjectOutputStream(byteStream);
			output.writeObject(this.employeeController);
			output.flush();
			controllerBytes = byteStream.toByteArray();
			this.prefs.putByteArray(CONTROLLER_KEY, controllerBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Restores the previous state of the employee controller.
	 * 
	 */
	private void restoreUIState() {
		byte[] controllerBytes = this.prefs.getByteArray(CONTROLLER_KEY, null);
		if (controllerBytes != null) {
			try {
				ByteArrayInputStream input = new ByteArrayInputStream(controllerBytes);
				ObjectInputStream objectInput = new ObjectInputStream(input);
				this.employeeController = (EmployeeController) objectInput.readObject();
				this.updateListView(this.employeeController);
			} catch (IOException | ClassNotFoundException e) {
				this.showErrorMessage(e);
			}
		} else {
			this.employeeController = new EmployeeController();
		}
	}

	private void close() {
		Stage current = (Stage) this.btnAddEmployee.getScene().getWindow();
		current.close();
	}

	private void showErrorMessage(Exception e) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setHeaderText("Error!");
		alert.setContentText(e.getMessage());
		alert.showAndWait();
	}

}