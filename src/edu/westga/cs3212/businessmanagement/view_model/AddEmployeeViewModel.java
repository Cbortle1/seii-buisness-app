package edu.westga.cs3212.businessmanagement.view_model;

import edu.westga.cs3212.businessmanagement.controller.EmployeeController;
import edu.westga.cs3212.businessmanagement.model.Employee;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddEmployeeViewModel {

	@FXML
	private Button btnAdd, btnBack, btnDone;

	@FXML
	private TextField txtName, txtWage;

	@FXML
	private ObservableList<Employee> mEmployees;

	private EmployeeController controller;

	private boolean employeeAdded, done;

	private static final int UNUSED_PIN = 1919;

	/**
	 * Initializes the employee controller and set employees added to false.
	 */
	@FXML
	public void initialize() {
		this.controller = new EmployeeController();
		this.employeeAdded = false;
		this.done = false;
	}

	/**
	 * Adds a new employee.
	 * 
	 * @param event
	 */
	@FXML
	public void add(ActionEvent event) {
		try {
			String name = this.txtName.getText();
			int wage = Integer.parseInt(this.txtWage.getText());
			this.controller.createAndAddEmployeeToHMap(name, UNUSED_PIN, wage);
			this.employeeAdded = true;
		} catch (Exception e) {
			this.showErrorMessage(e);
		}
	}

	/**
	 * Closes add employee dialog.
	 * 
	 */
	@FXML
	public void done() {
		this.done = true;
		this.close();
		
	}

	public boolean isDone() {
		return this.done;
	}

	/**
	 * Determines if the user has added a new employee,
	 * 
	 * @return True if the user has added a new employee, false otherwise.
	 */
	public boolean employeeAdded() {
		return this.employeeAdded;
	}

	/**
	 * Resets employeeAdded to false.
	 * 
	 */
	public void resetEmployeeAdded() {
		this.employeeAdded = false;
	}

	/**
	 * Returns the employee controller.
	 * 
	 * @return The employee controller.
	 */
	public EmployeeController getController() {
		return this.controller;
	}

	public void close() {
		Stage current = (Stage) this.btnAdd.getScene().getWindow();
		current.close();
	}

	private void showErrorMessage(Exception e) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setHeaderText("Error!");
		alert.setContentText(e.getMessage());
		alert.showAndWait();
	}

}
