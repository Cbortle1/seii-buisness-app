package edu.westga.cs3212.businessmanagement.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Stores a collection of Product Information.
 * 
 * @author Justin Allen
 *
 * @version 1.0
 */
public class ProfitLog implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4466951693881016058L;
	private ArrayList<Profit> profits;

	/**
	 * Creates a new ProductLog with a empty collection of profit information.
	 * 
	 * 
	 */
	public ProfitLog() {
		this.profits = new ArrayList<Profit>();
	}

	/**
	 * Adds new Profit information to the profit log.
	 * 
	 * @precondition profitInfo != null
	 * @postcondition 'profitInfo' will be added to the profit log
	 * 
	 * @param profitInfo
	 *            The new profit information to be added to the profit log.
	 * 
	 */
	public void addProfit(Profit profit) {
		if (profit == null) {
			throw new IllegalArgumentException("The profit info cannot be null");
		}
		this.profits.add(profit);
	}

	/**
	 * Removes Profit information from the profit log.
	 * 
	 * @precondition profitInfo != null
	 * @postcondition 'profitInfo' will be removed to the profit log
	 * 
	 * @param profitInfo
	 *            The selected profit information to be removed from the profit log.
	 * 
	 */
	public void removeProfitInfo(Profit profit) {
		if (profit == null) {
			throw new IllegalArgumentException("The profit info cannot be null");
		}
		this.profits.remove(profit);
	}

	/**
	 * Returns the number of entries in the profit log.
	 * 
	 * @return The number of entries in the profit log.
	 */
	public int size() {
		return this.profits.size();
	}

	/**
	 * Returns the Profit at the specified index.
	 * 
	 * @param index
	 *            The position of the profit information in the profit log.
	 * 
	 * @return The Profit at the specified index 'index'.
	 */
	public Profit get(int index) {
		return this.profits.get(index);
	}

	public ArrayList<Profit> getAllProfits() {
		return this.profits;
	}

	/**
	 * Returns a description of each entry in the profit log.
	 * 
	 * @return A description of each entry in the profit log.
	 */
	public String toString() {
		String result = "";
		if (this.profits.isEmpty()) {
			result = "The profit log is empty.";
		} else {
			for (Profit current : this.profits) {
				result += current.toString() + "\n";
			}
		}
		return result;
	}

}
