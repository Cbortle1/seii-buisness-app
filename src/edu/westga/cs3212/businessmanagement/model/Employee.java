package edu.westga.cs3212.businessmanagement.model;

import java.io.Serializable;

/**
 * 
 * @author Okami Apple
 *
 */
public class Employee implements Comparable<Employee>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8563450535167837903L;
	private int id;
	private String name;
	private Integer pin;
	private int wage;

	/**
	 * Constructor for the Employee Class
	 * 
	 * @param id
	 *            - The employee ID Number
	 * @param name
	 *            - the name of the employee
	 * 
	 * @precondition id> 0 && name != null
	 * @postcondition the Employee object is created
	 * 
	 */
	public Employee(int id, String name, int wage) {

		if (id < 0) {
			this.id = 10099;
		} else {
			this.id = id;
		}

		if (name == null) {
			this.name = "Er: Invalid Name";
		} else {
			this.name = name;
		}

		if (wage > 0) {
			this.wage = wage;
		} else {
			this.wage = 0;
		}
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the wage
	 */
	public int getWage() {
		return this.wage;
	}

	/**
	 * Sets pin for employee
	 * 
	 * @precondition pin must be over 3 digits long
	 * @param pin
	 *            the pin to set
	 * 
	 * @postcondition: sets the pin of the employee to a 4 or more digit pin
	 */
	public void setPin(int pin) {
		if (pin >= 1000) {
			this.pin = pin;
		} else {
			throw new IllegalArgumentException("Pin Must be atleast 4 numbers long");
		}
	}

	/**
	 * Gets the employee Pin
	 * @return the pin for the employee
	 */
	public Integer getPin() {
		return this.pin;
	}

	@Override
	public int compareTo(Employee otherEmployee) {
		return this.name.compareTo(otherEmployee.getName());
	}

	/**
	 * Description of employee.
	 * 
	 * @return string describing employee
	 */
	public String toString() {
		return "ID: " + this.id + "        NAME: " + this.name;
	}

}
