package edu.westga.cs3212.businessmanagement.model;

import java.util.UUID;

/**
 * The Product class represents products that will be sold. Each product ha a
 * name, price, id, and quantity.
 * 
 * @author Justin Allen
 *
 * @version 1.0
 */
public class Product {

	private String name;
	private double price;
	private int quantity;
	private UUID id;

	/**
	 * Creates a new product with a name, price, quantity, and id.
	 * 
	 * @precondition name != null && name.length() > 0
	 * @precondition price > 0
	 * @precondition quantity > 0
	 * @precondition id.length() == 4
	 * 
	 * @postcondition The product will be initialized with a name 'name', price
	 *                'price', quantity 'quantity', and id 'id'.
	 * 
	 * @param name
	 *            The name of the product.
	 * @param price
	 *            The price of the product per unit
	 * @param quantity
	 *            The quantity of the product
	 * @param id
	 *            The product's unique id
	 */
	public Product(String name, double price, int quantity, UUID id) {
		if (name == null || name.length() == 0) {
			throw new IllegalArgumentException("The name cannot be null or empty.");
		}
		if (price < 0) {
			throw new IllegalArgumentException("The price cannot be negative.");
		}
		if (quantity < 0) {
			throw new IllegalArgumentException("The quantity must be positive.");
		}

		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.id = id;
	}

	/**
	 * Returns the product's name.
	 * 
	 * @return The product's name.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the product's price.
	 * 
	 * @return The product's price.
	 */
	public double getPrice() {
		return this.price;
	}

	/**
	 * Returns the product's quantity.
	 * 
	 * @return The product's quantity.
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Returns the product's id.
	 * 
	 * @return The product's id.
	 */
	public UUID getId() {
		return this.id;
	}

	/**
	 * Sets the product's id.
	 * 
	 * @precondition id != null && id.length == 4
	 * 
	 * @postcondition The product's id will be set to 'id'.
	 * 
	 * @param id
	 *            The product's id.
	 */
	public void setId(String id) {
		if (id == null) {
			throw new IllegalArgumentException("the id cannot be null");
		}
		if (id.length() != 4) {
			throw new IllegalArgumentException("The id must be 4 characters.");
		}
		this.id = UUID.fromString(id);
	}

}
