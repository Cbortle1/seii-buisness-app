package edu.westga.cs3212.businessmanagement.model;

import java.io.Serializable;

/**
 * Stores and retrieves profit data received
 * 
 * @author Group 3
 * @version Spring 2018
 */
public class Profit implements Serializable {

	private static final long serialVersionUID = 2451005186872260228L;
	private String date;
	private double profit;

	/**
	 * Initializes ProductInformation with a date and corresponding profit.
	 * 
	 * @precondition date != null && profit !<= 0 && date.length !<= 0
	 * @postcondition ProductInformation is initialized && date and profits and have
	 *                been stored
	 * @param date
	 *            the date the profit information corresponds to
	 * @param profit
	 *            the profit made over the course of a day
	 */
	public Profit(String date, double profit) {
		if (profit <= 0) {
			throw new IllegalArgumentException("profit cannot be equal to or less than 0");
		}
		if (date == null || date.isEmpty()) {
			throw new IllegalArgumentException("date cannot be null or empty");
		}

		this.profit = profit;
		this.date = date;
	}

	/**
	 * Returns the stored profits.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the stored profits.
	 */
	public double getProfit() {
		return this.profit;
	}

	/**
	 * Returns the stored date.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the stored date.
	 */
	public String getDate() {
		return this.date;
	}

	/**
	 * Returns a String representation of the ProductInformation.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a String representation of the ProductInformation.
	 */
	public String toString() {
		return "DATE: " + this.date + "        PROFIT: " + this.profit;
	}
}
