package edu.westga.cs3212.businessmanagement.tests.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3212.businessmanagement.model.Profit;

/**
 * Tests the constructor of the Profit class.
 * 
 * @author James Harley Irwin
 * @version Sprint 2018
 */
class ProfitWhenProfitIsCreated {

	/**
	 * Tests that when valid information is used in the constructor, the
	 * Profit is created.
	 */
	@Test
	void testProfitWhenProfitIsCreatedWithZeroProfit() {
		Profit profit = new Profit("Not Null", 0);
		assert (profit.toString().equals("DATE: NOT NULL        PROFIT: 0"));
	}

	@Test
	void testProfitWhenProfitIsCreatedWith1000Profit() {
		Profit profit = new Profit("Not Null", 1000);
		assert (profit.toString().equals("DATE: NOT NULL        PROFIT: 1000"));
	}
	
	@Test
	void testProfitWhenProfitIsCreatedWithNullDate() {
		Profit profit = new Profit(null, 1000);
		assert (profit.toString().equals("DATE: NOT NULL        PROFIT: 1000"));
	}
	
	@Test
	void testProfitWhenProfitIsCreatedWithEmptyDate() {
		Profit profit = new Profit("", 0);
		assert (profit.toString().equals("DATE:       PROFIT: 0"));
	}
	
	@Test
	void testProfitWhenProfitIsCreatedWithSpecifiedDate() {
		Profit profit = new Profit("Now", 1000);
		assert (profit.getDate().equals("Now"));
	}
	
	@Test
	void testProfitGeProfitWhenProfitIsCreatedWithZeroProfit() {
		Profit profit = new Profit("Not Null", 0);
		assertEquals(0.0, profit.getProfit(), 0.001);
	}
}
