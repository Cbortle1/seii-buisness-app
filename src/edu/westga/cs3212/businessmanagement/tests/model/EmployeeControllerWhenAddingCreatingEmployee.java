package edu.westga.cs3212.businessmanagement.tests.model;

import org.junit.jupiter.api.Test;

import edu.westga.cs3212.businessmanagement.controller.EmployeeController;

/**
 * @author Okami Apple
 *
 */
class EmployeeControllerWhenAddingCreatingEmployee {

	/**
	 * Test method for
	 * {@link edu.westga.cs3212.businessmanagement.controller.EmployeeController#createAndAddEmployeeToHMap(java.lang.String, int)}.
	 */
	@Test
	void testCreateAndAddEmployeeToHMapNoEmployeesToOne() {
		EmployeeController ec = new EmployeeController();
		ec.createAndAddEmployeeToHMap("Bob Johnson", 1092, 0);
		assert (ec.toString().equals("HMap has: 1 Employees."));
	}

	@Test
	void testCreateAndAddEmployeeToHMapOneEmployeesToTwo() {
		EmployeeController ec = new EmployeeController();
		ec.createAndAddEmployeeToHMap("Bob Johnson", 1092, 0);
		ec.createAndAddEmployeeToHMap("Bob John2on", 1022, 0);
		assert (ec.toString().equals("HMap has: 2 Employees."));
	}

	@Test
	void testCreateAndAddEmployeeToHMapPopulateWith50Employees() {
		EmployeeController ec = new EmployeeController();
		for (int i = 0; i < 50; i++) {
			ec.createAndAddEmployeeToHMap("Bob Johnson", 1092, i);
		}
		assert (ec.toString().equals("HMap has: 50 Employees."));
	}

}
