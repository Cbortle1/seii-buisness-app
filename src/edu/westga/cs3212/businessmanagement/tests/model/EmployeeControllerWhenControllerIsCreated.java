package edu.westga.cs3212.businessmanagement.tests.model;

import org.junit.jupiter.api.Test;

import edu.westga.cs3212.businessmanagement.controller.EmployeeController;

/**
 * @author Okami Apple
 *
 */
class EmployeeControllerWhenControllerIsCreated {

	/**
	 * Test method for
	 * {@link edu.westga.cs3212.businessmanagement.controller.EmployeeController#EmployeeController()}.
	 */
	@Test
	void testEmployeeControllerWhenControllerIsCreated() {
		EmployeeController ec = new EmployeeController();
		assert (ec.toString().equals("HMap has: 0 Employees."));
	}

}
