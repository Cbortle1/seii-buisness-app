package edu.westga.cs3212.businessmanagement.tests.model;

import org.junit.jupiter.api.Test;

import edu.westga.cs3212.businessmanagement.model.Employee;

/**
 * @author Okami Apple
 *
 */
class EmployeeWhenEmployeeIsCreated {

	/**
	 * Test method for
	 * {@link edu.westga.cs3212.businessmanagement.model.Employee#Employee(int, java.lang.String)}.
	 */
	@Test
	void testEmployeeWhenEmployeeIsCreatedNoBoundraies() {
		Employee employee = new Employee(2301, "Not Null", 0);
		assert (employee.toString().equals("ID: 2301        NAME: Not Null"));
	}

	@Test
	void testEmployeeWhenEmployeeIsCreated1AboveBoundrayOnInt() {
		Employee employee = new Employee(1, "Not Null", 0);
		assert (employee.toString().equals("ID: 1        NAME: Not Null"));
	}

	@Test
	void testEmployeeWhenEmployeeIsCreatedWithBoundrayBrokenOnInt() {
		Employee employee = new Employee((-1), "Not Null", 0);
		assert (employee.toString().equals("ID: -1        NAME: Not Null"));
	}

	@Test
	void testEmployeeWhenEmployeeIsCreatedWithNullName() {
		Employee employee = new Employee(1234, null, 0);
		assert (employee.toString().equals("ID: 1234        NAME: Not Null"));
	}

	@Test
	void testEmployeeWhenEmployeeIsCreatedWithNullNameAndBoundrayBreakOnInt() {
		Employee employee = new Employee((-1), null, 0);
		assert (employee.toString().equals("10099-Er: Invalid Name Yearly Wage: 0"));
	}

	@Test
	void testEmployeeWhenEmployeeWageIsGreaterThanZero() {
		Employee employee = new Employee((-1), null, 100);
		assert (employee.toString().equals("10099-Er: 100"));
	}

	@Test
	void testEmployeeWhenEmployeeIdIsLessThan1000() {
		Employee employee = new Employee(999, null, 100);
		assert (employee.toString().equals("10099-Er: 100"));
	}

	@Test
	void testEmployeeWhenEmployeeWageIsEqualTo1000() {
		Employee employee = new Employee(1000, "Max", 1000);
		assert (employee.getWage() == 1000);
	}
	
	@Test
	void testEmployeeWhenEmployeeIdIsEqualTo1000() {
		Employee employee = new Employee(1000, null, 100);
		assert (employee.getId() == 1000);
	}
	
	@Test
	void testEmployeeWhenEmployePinInIsEqualTo999() {
		Employee employee = new Employee(1000, null, 100);
		employee.setPin(1000);
		assert (employee.toString().equals("1000: 100"));
	}
	
	@Test
	void testEmployeeWhenEmployePinInIsEqualToNegative1() {
		Employee employee = new Employee(1000, null, 100);
		employee.setPin(-1);
		assert (employee.toString().equals("10099-Er: 100"));
	}
	
	@Test
	void testEmployeeWhenEmployePinInIsEqualTo1500() {
		Employee employee = new Employee(1000, null, 100);
		employee.setPin(1500);
		assert (employee.getPin() == 1500);
	}

	@Test
	void testEmployeeWhenEmployeeListedBeforeSecondEmployee() {
		Employee employee = new Employee((-1), "Sam", 0);
		Employee employee2 = new Employee((-1), "Bob", 0);
		assert (employee.compareTo(employee2) == -1);
	}

}
