package edu.westga.cs3212.businessmanagement.parsers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import edu.westga.cs3212.businessmanagement.model.Employee;
import edu.westga.cs3212.businessmanagement.model.Profit;

/**
 * Parses needed data (Currently Employee Objects) into Json representations to
 * be sent a long the server and also to be stored in local repo/database files
 * 
 * @author Chance Bortle
 *
 */
public class JsonParser {

	private HashMap<Integer, Employee> employeeParseList;
	private ArrayList<String> employeeJsonList;
	private ArrayList<Profit> profitParseList;
	private ArrayList<String> profitJsonList;

	/**
	 * Constructor for the Json Parser
	 * 
	 * @precondition employeelist Not Null
	 * @postcondition JsonParser is created
	 * @param employeeList
	 *            - The hasmap of the employee list to add and parse
	 */
	public JsonParser(HashMap<Integer, Employee> employeeList) {
		if (employeeList == null) {
			throw new IllegalArgumentException("Er: Null Employee List.");
		}
		this.employeeParseList = employeeList;
	}

	public JsonParser(ArrayList<Profit> profitList) {
		if (profitList == null) {
			throw new IllegalArgumentException("Er: Null Profit List.");
		}
		this.profitParseList = profitList;
	}

	/**
	 * Converts EmployeeList to Json
	 */
	public void convertEmployeeListToJson() {
		for (Integer employeeKey : this.employeeParseList.keySet()) {
			Employee employeeToConvert = this.employeeParseList.get(employeeKey);
			this.employeeJsonList.add(this.convertToJsonForEmployee(employeeToConvert));
		}
	}

	/**
	 * Converts Profit List To Json
	 */
	public void convertProfitListToJson() {
		for (Profit profit : this.profitParseList) {
			this.profitJsonList.add(this.convertToJsonForProfit(profit));
		}
	}

	/**
	 * Saves Employee JSON List to file
	 */
	public void saveEmployeeJsonToFile() {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("EmployeeJson.json"))) {
			for (String line : this.employeeJsonList) {
				bw.write(line + "\n");
			}

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Saves Profit JSON List to file
	 */
	public void saveProfitJsonToFile() {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("ProfitJson.json"))) {
			for (String line : this.profitJsonList) {
				bw.write(line + "\n");
			}

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String convertToJsonForEmployee(Employee employeeToConvert) {
		String output = "{ \"id\":" + employeeToConvert.getId() + ", \"name\":" + employeeToConvert.getName()
				+ ", \"pin\":" + employeeToConvert.getPin() + ", \"wage\":" + employeeToConvert.getWage() + " }";
		return output;
	}

	private String convertToJsonForProfit(Profit profitToConvert) {
		String output = "{ \"date\":" + profitToConvert.getDate() + ", \"profit\":" + profitToConvert.getProfit()
				+ " }";
		return output;
	}

}
