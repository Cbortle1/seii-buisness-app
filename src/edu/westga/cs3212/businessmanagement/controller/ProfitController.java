package edu.westga.cs3212.businessmanagement.controller;

import java.util.ArrayList;

import edu.westga.cs3212.businessmanagement.model.Profit;
import edu.westga.cs3212.businessmanagement.model.ProfitLog;

/**
 * Design Controller class that will parse information from TUI.
 * 
 * @author Group 3
 * @version Spring 2018
 */
public class ProfitController {

	private ProfitLog profitLog;

	/**
	 * Initializes the ProfitController to begin holding Profit.
	 * 
	 * @precondition none
	 * @postcondition ProfitController is initialized with an empty ProfitLog
	 */
	public ProfitController() {
		this.profitLog = new ProfitLog();
	}

	/**
	 * Takes information given by the TUI and stores it as Profit
	 * 
	 * @precondition none
	 * @postcondition Profit is generated and stored
	 */
	public void processProfit(String date, double price) {
		Profit aProfit = new Profit(date, price);

		this.profitLog.addProfit(aProfit);
	}

	/**
	 * Returns a string representation of all the Profit entered into the
	 * system.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a string representation of all the Profit entered into the
	 *         system.
	 */
	public String returnProfit() {
		return this.profitLog.toString();
	}
	
	/**
	 * Gets a list of all Profits in the profit log.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a list of all Profits in the profit log
	 */
	public ArrayList<Profit> getProfits() {
		return this.profitLog.getAllProfits();
	}
}
