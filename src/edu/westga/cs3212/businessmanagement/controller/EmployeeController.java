package edu.westga.cs3212.businessmanagement.controller;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;

import edu.westga.cs3212.businessmanagement.model.Employee;

/**
 * @author Okami Apple
 *
 */
public class EmployeeController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8943479584075018008L;
	private HashMap<Integer, Employee> EmployeeList;

	/**
	 * Constructor for the EmployeeController class This class manages and deals
	 * with the intricacies of the employee class (This will be expanded as we add
	 * features to be a good JDoc of this class)
	 * 
	 * @param none
	 * @precondition none
	 * @postcondition The employee Controller class is created
	 * 
	 */
	public EmployeeController() {
		// Integer is ID Number, String is name
		this.EmployeeList = new HashMap<Integer, Employee>();
	}

	// region publicMethods
	// -----------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * Removes Employee by Their ID Number from the Hashmap
	 * 
	 * @param IdNumber
	 *            ID Number to remove
	 */
	public void removeEmployeeByID(int id) {
		if (this.EmployeeList.containsKey(id)) {
			this.EmployeeList.remove(id);
		} else {
			throw new IllegalArgumentException("Er: Cannot remove, No Matching Employee ID Number.");
		}
	}

	// Im not testing these yet, im not sure which one we should use, because we can
	// enforce either of them on the GUI level.... But they are simple methods so
	// they work

	/**
	 * Removes Employee by Their ID Number & Name for extra security from the
	 * Hashmap
	 * 
	 * @param id
	 *            id to remove
	 * @param name
	 *            name to remove
	 */
	public void removeEmployeeByNameAndID(int id, Employee employee) {
		if (this.EmployeeList.containsValue(employee)) {
			this.EmployeeList.remove(id, employee);
		} else {
			throw new IllegalArgumentException(
					"Er: Cannot remove, No Matching Employee ID Number or No Matching Employee Name.");
		}
	}

	/**
	 * Creates, and adds a new employee to the Hashmap.
	 * 
	 * @param employeeName
	 *            Name of employee
	 * @param employeePin
	 *            Pin number they will choose
	 */
	public void createAndAddEmployeeToHMap(String name, int pin, int wage) {
		if (pin > 999 && name != null && wage > 0) {
			this.addEmployeeToHMap(this.createEmployee(name, pin, wage));
		} else {
			throw new IllegalArgumentException("Employee Name or Pin were invalid, please try again");
		}

	}

	public Collection<Employee> getEmployees() {
		return this.EmployeeList.values();
	}
	
	public HashMap<Integer, Employee> getMap() {
		return this.EmployeeList;
	}
	
	public void addAllFrom(EmployeeController controller) {
		this.EmployeeList.putAll(controller.getMap());
	}

	// end region publicMethods
	// -----------------------------------------------------------------------------------------------------------------------------------------------------

	// region privateMethods
	// --------------------------------------------------------------------------------------------------------------------------------------------------------

	private int generateIdNumber() {
		Random random = new Random();
		return random.nextInt(999999);
	}

	private Employee createEmployee(String name, int pin, int wage) {

		// Generates ID Number for the Employee
		int id = -1;
		if (!this.EmployeeList.containsKey(id)) {
			id = this.generateIdNumber();
		} else {
			id = 10099;
		}

		Employee newEmployee = new Employee(id, name, wage);
		newEmployee.setPin(pin);

		return newEmployee;

	}

	private void addEmployeeToHMap(Employee employeeToAdd) {

		if (employeeToAdd.equals(null)) {
			throw new IllegalArgumentException("Er: Cannot add Null Employee object.");
		} else {
			EmployeeList.put(employeeToAdd.getId(), employeeToAdd);
		}

	}

	public String toString() {
		String output = "HMap has: " + this.EmployeeList.size() + " Employees.";
		System.out.println("/n" + this.EmployeeList.toString());
		System.out.println(output);
		return output;
	}

}
